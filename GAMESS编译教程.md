GAMESS是一款历史悠久、免费开源的量子化学软件，现今最流行的分支是GAMESS-US（下文简称GAMESS），该程序的最新版可在量化大佬[Mark Gordon课题组主页](https://www.msg.chem.iastate.edu/gamess/download.html)上申请，同意协议、填写邮箱等信息后一两天内会收到下载链接、账户名和下载密码。若想要以往版本的GAMESS，可在计算化学公社论坛[这个帖子](http://bbs.keinsci.com/thread-727-1-1.html)里找到。

本文以2024 R2 Patch 1为例展示安装过程。对于比2019 R1更旧的GAMESS版本，读者应按照[GAMESS编译教程](https://mp.weixin.qq.com/s/w689PJc4c2H8sWj03zsUBA)来编译。


## 1.解压
将压缩包发送到Linux系统上，解压
```
tar -zxf gamess-current.tar.gz
```
通常情况下解压出的是一个gamess目录，内含`README.md`文件，`VERSION.md`文件，`src`目录等等。


## 2.配置
进入gamess目录，执行`./config`进行配置。这一步是交互式命令行选择配置信息。注意，不同版本的GAMESS在此处会有两三个问题的小差异，请读者仔细阅读屏幕提示，随机应变，举一反三。这里展示一下笔者的安装过程

（1）按Enter键  
（2）填写linux64后按Enter  
（3）设置安装路径，笔者解压前已放到自己想安装的位置，直接按Enter  
（4）按Enter  
（5）设置可执行程序版本号，一般就用默认的00，直接按Enter即可。这里的数字00对应最后编译完生成的gamess.00.x可执行文件  
（6）按Enter  
（7）选择编译器，填ifort（这也是绝大多数人的选择），然后按Enter确认。有专人管理维护的集群几乎都装有Intel编译器，例如集群上运行`module avai`查看是否有相应的Intel编译器可直接加载。个人机器或当前节点可运行`which ifort`查看情况  
（8）填写ifort版本号，笔者用的是Intel 2021，因此写21。这可以通过运行`ifort --version`查看。然后按Enter  
（9）按Enter  
（10）选择数学库类型，填mkl（这也是绝大多数人的选择），然后按Enter确认  
（11）填写数学库路径。一般来说只要Intel编译器正确安装、写好了环境变量，此时屏幕上就会显示MKL路径（在Found:后面），按Enter  
（12）按Enter  
（13）选择并行类型，笔者习惯使用sockets，即输入sockets，按Enter  
（14）依次询问需要安装什么库，笔者都不用上，全都选no。这一步配置的作用是产生Makefile和install.info两个文件。GAMESS文档中强调从已装好GAMESS的机器上把这两个文件复制过来是不行的


## 3.编译
从这里开始与旧版不同，有很大简化。执行一行命令即可
```
make -j8
```
旧版GAMESS的编译里，需要用户手动去移动ddikick.x文件，如今执行make后它直接就在合适的目录下。数字8表示8核并行编译，读者请根据自己实际情况修改。旧版要手动执行`/.lked`链接成gamess.00.x，现今也不需要了。


## 4.修改rungms脚本
`rungms`脚本就在`gamess/`目录下，只需修改这个文件中的如下三行为自己的实际路径
```
set SCR=/scratch/jxzou/gamess
set USERSCR=/scratch/jxzou/gamess
set GMSPATH=/home/jxzou/software/gamess
```
注意，`SCR`和`USERSCR`应当是真实存在的合理路径。若不存在，则需自己创建相应的目录。这里`jxzou`是笔者的用户名，读者请自行修改。有的集群会在机器上安装固态硬盘并将其挂载到`/scratch`，以加速临时文件读写。若读者所用机器没有这种条件，则不必照抄写`/scratch`，应自行设置合适路径。为方便以后在任意目录下都能简易运行rungms脚本，可以在自己的`~/.bashrc`文件里设置以下任一种环境变量
```
export GMS=/home/jxzou/software/gamess/rungms
alias gms='/home/jxzou/software/gamess/rungms'
```
二者区别在于alias别名只能在当前节点上直接使用，且在bash脚本或Fortran代码`system()`中是无法自动识别别名的。而`$GMS`既可以被bash脚本或Fortran代码识别，也可以在任何共享~/.bashrc文件的节点上使用，比如集群登录节点和计算节点。注意别把这两行误写进rungms脚本。这样以后使用`$GMS`或`gms`即代表rungms脚本。


## 5.测试
测试GAMESS自带示例文件。运行
```
./runall 00
```
数字00是我们之前填写的版本号，约5 min测试完成。若不到30 s就结束了，可以随便打开一个如exam01.log文件查看结尾的报错信息，通常是用户在上述安装过程中某一步不小心弄错了。在当前目录下，执行
```
./tests/standard/checktst
```
注意一开始有个点号。此即检查上述测试结果是否正常。笔者机子上49个例子全都能通过。可以将这49个log文件以及上述USERSCR目录下的一堆临时文件全部删除。若想单独测试rungms脚本，可以到tests/standard/下找个文件尝试，以exam01.inp文件为例，运行格式为
```
$GMS exam01.inp 00 2 >exam01.gms 2>&1 &
```
此处假设在第4步中已经设置了`$GMS`环境变量。00即为版本号，2代表并行核数，可以根据自己的需求修改。顺带一提，GAMESS程序手册就是gamess/目录里那些docs-开头的txt文件。如果嫌看得累，可以复制到本地转成PDF格式再阅读。


## 6.从Gaussian传轨道给GAMESS
我们算一个略有挑战性的体系，高锰酸根负离子MnO4-，在UHF/aug-cc-pVDZ水平下做个单点计算，先给出高斯输入文件
```
%chk=MnO4-_aug-cc-pVDZ_uhf.chk
%mem=96GB
%nprocshared=48
#p UHF/aug-cc-pVDZ nosymm int=nobasistransform guess=mix stable=opt

title

-1 1
Mn     0.00000000    0.00000000    0.00000000
O      0.00000000    0.00000000    1.60200000
O      1.51038009    0.00000000   -0.53400000
O     -0.75519005    1.30802752   -0.53400000
O     -0.75519005   -1.30802752   -0.53400000

```
算完后依次执行
```
formchk MnO4-_aug-cc-pVDZ_uhf.chk MnO4-_aug-cc-pVDZ_uhf.fch
fch2inp MnO4-_aug-cc-pVDZ_uhf.fch
```
即可生成GAMESS输入文件`MnO4-_aug-cc-pVDZ_uhf.inp`，内含关键词、坐标、基组和轨道信息。`formchk`是Gaussian自带的小程序，而[fch2inp](https://jeanwsr.gitlab.io/mokit-doc-mdbook/chap4-5.html#4522-fch2inp)是笔者开发的[MOKIT](https://gitlab.com/jxzou/mokit)中的一个小程序，功能是从高斯向GAMESS传轨道。关键词`nosymm int=nobasistransform`是为保证传轨道的正确性写的，若不传轨道，则不用写。如果要用DFT的话用户需要自己添加泛函名称，而此处我们用的是UHF，不需要修改自动生成的文件。提交给GAMESS做计算，即运行
```
$GMS MnO4-_aug-cc-pVDZ_uhf.inp 00 16 >MnO4-_aug-cc-pVDZ_uhf.gms 2>&1 &
```
结果如下
```
 ITER EX      TOTAL ENERGY        E CHANGE  DENSITY CHANGE    ORB. GRAD       INTEGRALS    SKIPPED
   1  0    -1449.0296694169 -1449.0296694169   0.000001311   0.000000000       85173387      13043
          ---------------START SECOND ORDER SCF---------------
   2  1    -1449.0296694174    -0.0000000005   0.000000324   0.000000018       68998486     308262

          -----------------
          DENSITY CONVERGED
          -----------------
     TIME TO FORM FOCK OPERATORS=       4.6 SECONDS (       2.3 SEC/ITER)
     FOCK TIME ON FIRST ITERATION=       2.4, LAST ITERATION=       2.1
     TIME TO SOLVE SCF EQUATIONS=       0.0 SECONDS (       0.0 SEC/ITER)

 FINAL UHF ENERGY IS    -1449.0296694174 AFTER   2 ITERATIONS
```
立即就收敛了，说明传的轨道十分准确。而这个例子用GAMESS算会面临2个困难：（1）GAMESS内置基组对Mn支持aug-cc-pVTZ，却不支持aug-cc-pVDZ。（2）即使用户使用自定义基组，由于GAMESS的SCF收敛性远不如高斯，能量会以几十、上百Hartree地振荡，最终不收敛。当然，换成aug-cc-pVTZ就更难算了。当我们在GAMESS中碰到SCF不收敛时，除了去翻程序手册尝试帮助收敛的关键词外，还可直接用`fch2inp`小程序从高斯fch文件传轨道给GAMESS。


## 7.与其他程序适配
读者可根据自己的需求选择合适的修改方式。

### 7.1 MOKIT
MOKIT可以自动构造多组态方法GVB的初始轨道，传给GAMESS，并调用其进行GVB计算。而GAMESS代码中将GVB活性空间“对的数目”（number of pairs)写为固定的12。早年间GVB计算非常困难，几乎不会计算12对以上的体系，但却不符合如今的需求了，为此我们需要修改GAMESS代码。先按上述步骤正常编译，然后运行
```
cd gamess/source
cp $MOKIT_ROOT/src/modify_GMS1.sh .
cp $MOKIT_ROOT/src/modify_GMS2.f90 .
./modify_GMS1.sh
```
接着按`y`确认修改。对于通过`conda`联网安装MOKIT的用户，可到GitLab下载[两个文件](https://gitlab.com/jxzou/mokit/-/tree/master/src)后传到服务器上。完成后回到上级目录，会发现新生成了`gamess.01.x`可执行文件。

### 7.2 XEDA
GKS-EDA方法第一个大版本的实现是以xeda插件方式修改GAMESS。它要求在编译GAMESS前就修改代码
```
mv gamess gamess_xeda
cd gamess_xeda/
../xeda-patch -v 2021-R2
```
这里笔者将xeda-patch脚本放在了上级目录，所以可以用`../`来运行。如果读者将其放在其他目录，则应写清楚路径。如果读者用的不是2021-R2，应在上述命令中替换相应的版本号。修改完之后，按上述步骤正常编译GAMESS。安装完后，笔者推荐在~/.bashrc中添加（以下二选一）
```
export XEDA=$HOME/software/gamess_xeda/rungms
alias xeda=$HOME/software/gamess_xeda/rungms
```


## 8.可能遇到的问题
若读者使用的是Ubuntu系统，且运行
```
ls -l /usr/bin/csh
```
发现没有csh，或存在csh但它没有指向tcsh，则GAMESS无法使用。此时有两种解决办法：

（1）机器可以联网  
运行
```
sudo apt remove csh
sudo apt install tcsh
```
解决问题。

（2）机器不能/不允许联网  
到如下两个网站下载ncurses-6.1.tar.gz和tcsh-6.24.00.tar.gz压缩包
```
https://astron.com/pub/tcsh
https://ftp.gnu.org/gnu/ncurses
```
版本可以用更新的，但不建议更旧。上传至服务器。

编译ncurses
```
tar -zxf ncurses-6.1.tar.gz
cd ncurses-6.1/
./configure --prefix=$HOME/software/ncurses6 --with-shared
make -j64
make install
```
在个人~/.bashrc中写ncurses环境变量
```
export PATH=$HOME/software/ncurses6/bin:$PATH
export LD_LIBRARY_PATH=$HOME/software/ncurses6/lib:$LD_LIBRARY_PATH
export CPATH=$HOME/software/ncurses6/include:$CPATH
```
退出重登，使环境变量生效。

接着编译tcsh
```
tar -zxf tcsh-6.24.00.tar.gz
cd tcsh-6.24.00/
mkdir build
cd build/

export LDFLAGS="-L$HOME/software/ncurses6/lib"
../configure --prefix=$HOME/software/tcsh6
make -j64
make install

ln -s $HOME/software/tcsh6/bin/tcsh $HOME/software/tcsh6/bin/csh
```
注意中间有一行`export LDFLAGS`不要漏了。在个人~/.bashrc中写tcsh环境变量
```
export PATH=$HOME/software/tcsh6/bin:$PATH
```
退出重登，使环境变量生效。运行
```
which csh
```
发现已经有csh，并且在自己编译的路径下。打开gamess/rungms文件，将第一行
```
#!/bin/csh -f
```
修改为
```
#!/home/jxzou/software/tcsh6/bin/csh -f
```
注意，jxzou是笔者用户名，具体路径请根据自己的用户名和安装路径修改。


## 相关阅读
1. [GAMESS编译教程](https://mp.weixin.qq.com/s/w689PJc4c2H8sWj03zsUBA)  
2. [Ubuntu下使用ifort编译config不识别的解决方案](http://bbs.keinsci.com/thread-34171-1-1.html)
3. [用GAMESS中的Spin-flip TD-DFT找S0/S1交叉点](https://mp.weixin.qq.com/s/UY3Ixz6CO86XAeVGtuXn4w)  
4. [Spin-flip方法中RODFT难收敛解决办法](https://gitlab.com/jxzou/qcinstall/-/blob/main/Spin-flip%E6%96%B9%E6%B3%95%E4%B8%ADRODFT%E9%9A%BE%E6%94%B6%E6%95%9B%E8%A7%A3%E5%86%B3%E5%8A%9E%E6%B3%95.md?ref_type=heads)  
5. [GKS-EDA计算简介](https://mp.weixin.qq.com/s/6nuJpYJdNbUJndrYM13Fog)  
6. [《自动做多参考态计算的程序MOKIT》](https://mp.weixin.qq.com/s/bM244EiyhsYKwW5i8wq0TQ)
