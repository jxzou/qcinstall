Dalton是一款开源免费的量子化学程序，主要特色是支持一大堆性质的计算，例如DFT下的二次响应计算（常规TDDFT计算仅是线性相应），CASSCF波函数下的核磁NMR计算等等，以及各类激发态方法。当然，也有一些高精度单参考方法（如MP2-R12和CCSD-R12等），但较少用到。程序PDF手册下载网址
```
https://daltonprogram.org/documentation
```

## 0. 安装前提
cmake>=3.1。机子上需装有C、C++和Fortran编译器，BLAS和LAPACK数学库。

Dalton的并行有两种方式：MPI并行和MKL并行。若想编译前者，需提前安装任一种MPI（例如[openmpi](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E5%AE%89%E8%A3%85Open-MPI.md)）。若编译后者，则无需安装任何MPI。注意Dalton代码本身不支持OpenMP并行，靠其调用的数学库函数（MKL）等实现多线程计算。两种编译方式在下文都有介绍，读者选择一种即可。根据笔者的有限测试，对CASSCF NMR计算而言，MKL并行比MPI并行快一些；而耦合簇计算不支持MPI并行。

笔者撰文时用的是cmake 3.22.1、Intel OneAPI 2021.1（内含icc, icpc和ifort编译器和Intel MKL数学库），这些前提都满足。若读者机子未安装过Intel全家桶，对于2019或2020版本，可在如下两处
```
http://bbs.keinsci.com/thread-21841-1-1.html
http://bbs.keinsci.com/thread-12118-1-1.html
```
下载到压缩包。对于2021或更高版本，可阅读[《Linux下安装Intel oneAPI》](https://mp.weixin.qq.com/s/7pQETkrDO1C83vQjKQqI4w)。

## 1. 下载压缩包
到Dalton的[GitLab主页](https://gitlab.com/dalton/dalton/-/releases)下载源码，笔者下载的压缩包是dalton-Dalton2020.1.tar.gz。注意Dalton还依赖其他库，我们到如下三个网址分别下载三个库
```
https://gitlab.com/bingao/gen1int/-/tree/1e4148ecd676761b3399801acba443925a1fee6b
https://gitlab.com/pe-software/pelib/-/tree/19dd0e91afbd18b7c9f2611b9978b1aeaa1c19f9
https://github.com/cstein/qfitlib/tree/1acdc9863fdeae2cdbc7f5a599413257a095b8ad
```
笔者下载的压缩包是
```
gen1int-1e4148ecd676761b3399801acba443925a1fee6b.tar.gz
pelib-19dd0e91afbd18b7c9f2611b9978b1aeaa1c19f9.zip
qfitlib-1acdc9863fdeae2cdbc7f5a599413257a095b8ad.zip
```
注意这些库的版本是Dalton 2020.1安装配置推荐的，不建议使用其他版本。

## 2. 编译Dalton
解压，进入Dalton的依赖目录
```
tar -zxf dalton-Dalton2020.1.tar.gz
cd dalton-Dalton2020.1/external/
```

将下载好的三个库移动至此目录下，解压，重命名文件夹
```
mv ~/software/gen1int-1e4148ecd676761b3399801acba443925a1fee6b.tar.gz .
mv ~/software/pelib-19dd0e91afbd18b7c9f2611b9978b1aeaa1c19f9.zip .
mv ~/software/qfitlib-1acdc9863fdeae2cdbc7f5a599413257a095b8ad.zip .
rm -rf gen1int pelib qfitlib

tar -zxf gen1int-1e4148ecd676761b3399801acba443925a1fee6b.tar.gz
unzip pelib-19dd0e91afbd18b7c9f2611b9978b1aeaa1c19f9.zip
unzip qfitlib-1acdc9863fdeae2cdbc7f5a599413257a095b8ad.zip

mv gen1int-1e4148ecd676761b3399801acba443925a1fee6b gen1int
mv pelib-19dd0e91afbd18b7c9f2611b9978b1aeaa1c19f9 pelib
mv qfitlib-1acdc9863fdeae2cdbc7f5a599413257a095b8ad qfitlib

rm -rf gen1int-1e4148ecd676761b3399801acba443925a1fee6b.tar.gz
rm -rf pelib-19dd0e91afbd18b7c9f2611b9978b1aeaa1c19f9.zip
rm -rf qfitlib-1acdc9863fdeae2cdbc7f5a599413257a095b8ad.zip
```

这三个库会在编译Dalton时自动被检测并编译，因此这里我们直接到上级目录编译Dalton即可。以下两种方式分别对应MKL并行和MPI并行，读者**仅需选择一种方式**进行编译
```
cd ..
./setup --fc=ifort --cc=icc --cxx=icpc --mkl=parallel --int64 --prefix=$HOME/software
cd build
make -j16
make install
```

或
```
cd ..
./setup --fc=mpif90 --cc=mpicc --cxx=mpicxx --mkl=sequential --mpi --int64 --prefix=$HOME/software
cd build
make -j16
make install
```

其中`--prefix`指定存放可执行程序的路径，可按自己的偏好修改。注意末尾不需要写dalton，上述写法生成的目录即为`$HOME/software/dalton`。`-j16`表示用16核并行编译，耗时约3 min，读者请根据自己机子实际情况修改。如果没指定`--int64`，那在计算时最多只能使用16GB内存（上限大概对应CAS(12,12)的计算）。在上述第二种编译方式（即MPI并行）中，需要注意OpenMPI也要用Intel编译器编译。

## 3. 配置环境变量和测试
对于MKL并行版，在个人~/.bashrc中写入
```
export PATH=$HOME/software/dalton:$PATH
export DALTON_TMPDIR=/scratch/$USER/dalton
export OMP_NUM_THREADS=16
```
然后退出重登使之生效。第二行为Dalton临时文件路径，读者需根据自己机器实际情况进行修改。

对于MPI并行版，在个人~/.bashrc中写入
```
export PATH=$HOME/software/dalton:$PATH
export DALTON_TMPDIR=/scratch/$USER/dalton
export DALTON_LAUNCHER="mpirun -np 16"
```
然后退出重登使之生效。

至此，压缩包dalton-Dalton2020.1.tar.gz和解压文件夹dalton-Dalton2020.1已经没用了，可以删掉。若Dalton使用过程中有任何问题，可到以下两个网址发帖提问
```
https://gitlab.com/dalton/user-support
https://gitlab.com/dalton/dalton/-/issues
```

我们做一个水分子的CCSD(T)/cc-pVTZ计算，与高斯的结果进行对比。一般而言Dalton需要2个输入文件，展示如下  
h2o.dal文件内容
```
**DALTON INPUT
.RUN WAVE FUNCTIONS
**WAVE FUNCTIONS
.CC
*CC INPUT
.FREEZE
1 0
.CC(T)
**END OF INPUT
```

h2o.mol文件内容
```
BASIS
cc-pVTZ
 CCSD(T)/cc-pVTZ for H2O molecule
 No symmetry is used
AtomTypes=2 Integrals=1.0D-14 Charge=0 NoSymmetry Angstrom
Charge=8. Atoms=1
O    -0.78690803    0.04178273    0.0
Charge=1. Atoms=2
H_a   0.17309197    0.04178273    0.0
H_b  -1.10736261    0.94671856    0.0
```

提交任务
```
dalton -gb 8 -ow h2o
```
`-gb 8`表示使用8GB内存。由于我们之前已使用环境变量指定并行核数，这里可以不写。注意这个例子是耦合簇计算，不支持MPI并行，要使用MKL并行版。若想临时改变MKL并行核数，可以这么运行
```
dalton -gb 8 -omp 12 -ow h2o
```
注意高斯做电子相关计算默认冻核，但在Dalton中则默认不是，我们需要显式地将冻核数目写出来，此例中冻结最低的1个双占据轨道（即氧原子的1s轨道），两个程序算出的能量方能严格对比。对于冻核不了解的小伙伴可以看[《电子相关计算中的"冻核"近似》](https://mp.weixin.qq.com/s/zE-Xf7z5TKACye3R9s2YKQ)。算完后在输出文件h2o.out中找到CCSD(T)能量为-76.3313527295 a.u.，而Gaussian结果为-76.331352624 a.u.，符合得很好。高斯输入文件大家都很熟悉，就不展示了。

## 4. 从其他量化程序向Dalton传轨道
Dalton的SCF功能不算出众，有时候做一些较为复杂的计算，可能在一开始的HF/DFT步骤遇到不收敛，或者计算开壳层体系收敛到了高能解。还有的时候用户已经用Gaussian/ORCA/PySCF做了HF/DFT计算，那就不必在Dalton中从零开始算了，可通过传轨道给Dalton来节约时间，此功能请阅读以下三篇

[《利用MOKIT从Gaussian向其他量化程序传轨道》](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E5%88%A9%E7%94%A8MOKIT%E4%BB%8EGaussian%E5%90%91%E5%85%B6%E4%BB%96%E9%87%8F%E5%8C%96%E7%A8%8B%E5%BA%8F%E4%BC%A0%E8%BD%A8%E9%81%93.md)  
[《利用MOKIT从ORCA向其他量化程序传轨道》](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E5%88%A9%E7%94%A8MOKIT%E4%BB%8EORCA%E5%90%91%E5%85%B6%E4%BB%96%E9%87%8F%E5%8C%96%E7%A8%8B%E5%BA%8F%E4%BC%A0%E8%BD%A8%E9%81%93.md)  
[《利用MOKIT从PySCF向其他量化程序传轨道》](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E5%88%A9%E7%94%A8MOKIT%E4%BB%8EPySCF%E5%90%91%E5%85%B6%E4%BB%96%E9%87%8F%E5%8C%96%E7%A8%8B%E5%BA%8F%E4%BC%A0%E8%BD%A8%E9%81%93.md)

## 参考阅读
[1] https://dalton-installation.readthedocs.io/en/latest  
[2] http://sobereva.com/463  
[3] [MOKIT+Dalton联用实现CASSCF ICSS计算](https://jeanwsr.gitlab.io/mokit-doc-mdbook/chap5-4.html)

