本文以OpenMolcas-v24.10为例，对旧版OpenMolcas和v23.10版亦适用。安装前我们需要确保有必要的编译器和库：  
cmake （建议>= 3.18），gcc，Intel MKL，python。
<!--笔者机子上安装的分别是cmake 3.31、gcc 4.8.5和Miniconda3。-->
若读者机器上未安装cmake或安装了但版本过低，可以到[官网](https://cmake.org/download/)下载二进制版，解压写好环境变量即可使用。

# 1. 下载
分别到以下3个网址下载压缩包
```
https://gitlab.com/Molcas/OpenMolcas/-/releases
https://github.com/HDFGroup/hdf5/releases
https://github.com/SebWouters/CheMPS2/archive/refs/heads/master.zip
```
<!--
笔者下载的分别是
```
OpenMolcas-v24.10.tar.gz
hdf5-hdf5-1_14_1-2.tar.gz
```
-->
# 1. 安装HDF5
可与OpenMolcas使用同一个，参见 [安装hdf5](./离线安装OpenMolcas-v22.06.md#2-安装hdf5)

# 3. 编译chemps2
进入存放压缩包的目录，解压后，进入源码目录
```
cd chemps2-master
mkdir build
cd build
CXX=g++ cmake .. -DMKL=on -DCMAKE_INSTALL_PREFIX=$HOME/software/chemps2 -DWITH_MPI=off
make
make install
```
接着在~/.bashrc文件中导出环境变量
```
export PATH=$HOME/software/chemps2/bin:$PATH
export LD_LIBRARY_PATH=$HOME/software/chemps2/lib64:$LD_LIBRARY_PATH
```
执行`source ~/.bashrc`或退出重登以使环境变量生效。
测试是否可运行`chemps2 -h`.

# 3. 编译OpenMolcas
进入存放压缩包的目录，解压，配置
```
tar -zxf OpenMolcas-v24.10.tar.gz
cd OpenMolcas-v24.10
mkdir bin build
cd build
CC=gcc CXX=g++ FC=gfortran cmake -DLINALG=MKL -DOPENMP=on -DCMAKE_INSTALL_PREFIX=$HOME/software/OpenMolcas-v24.10 -DCHEMPS2=ON -DCHEMPS2_DIR=$HOME/software/chemps2/bin ..
```
如需要 Libxc 可参照 [离线安装OpenMolcas](./离线安装OpenMolcas-v22.06.md) 的相关内容处理。
```
make -j4
make install
```
正常结束后执行
```
cd ..
mv pymolcas bin/
```
即将脚本pymolcas移入我们之前新建的bin目录里。接着在~/.bashrc中写上OpenMolcas环境变量
```
# OpenMolcas-v24.10-chemps2
export MOLCAS_WORKDIR=/scratch/$USER/molcas
export MOLCAS_MEM=32Gb
export MOLCAS=$HOME/software/OpenMolcas-v24.10
export PATH=$MOLCAS/bin:$PATH
export MOLCAS_PRINT=3
export MOLCAS_NPROCS=1
export OMP_NUM_THREADS=24
```
完成后记得执行source ~/.bashrc或退出重登。各种路径可以根据读者自己机子的实际情况进行修改。若/scratch/$USER/molcas目录不存在则需手动创建。无论计算任务正常/异常结束，该目录都会有临时文件存在，每隔一段时间应进行清理。若一个任务算完后在相同位置再次提交，OpenMolcas默认去寻找上次的临时文件，好处是可以加速计算，坏处是万一不想要临时文件里的轨道作为初猜，就可能把自己坑了。若想每次计算完自动清空临时文件，可以再加上环境变量
```
export MOLCAS_KEEP_WORKDIR=NO
```
变量MOLCAS_PRINT=3可以让输出内容更多一些。变量MOLCAS_NPROCS用于MPI并行，但本文编译的是MKL并行版，不支持MPI并行，因此设为1。详细的环境变量说明请阅读OpenMolcas手册
```
https://molcas.gitlab.io/OpenMolcas/Manual.pdf
```

# 4. 测试
执行
```
pymolcas --version
```
屏幕应当显示
```
python driver version = py2.29
(after the original perl EMIL interpreter of Valera Veryazov)
```
接着测试程序自带标准示例。最好切换到其他目录进行测试
```
cd ~/
pymolcas verify extra:850,851
```
## 相关阅读
1. [OpenMolcas manual](https://molcas.gitlab.io/OpenMolcas/sphinx/installation.guide/dmrginst.html)


