安装CP2K程序对新手来说是一件略有挑战性的事。虽然GitHub上有预编译版下载即用，但其计算效率远不如在自己机器上编译的，因此自行编译仍较有必要。网上的CP2K安装教程多为使用高版本gcc编译器编译，笔者出于好奇尝试使用Intel编译器编译CP2K（仍需机器上有gcc编译器，只不过对gcc版本要求略低）。笔者撰写本文时所用Intel编译器是Intel OneAPI 2021.2，含Intel MKL，cmake为3.22.1，机子自带gcc-4.8.5。但CP2K依赖的ELPA库要求有C++17支持，而gcc-4.8.5版本较旧，不仅自身不支持C++17标准，还会影响mpic++导致ELPA编译报错，笔者撰写本文时加载的是gcc-9.2，之所以用加载这个词，是因为集群管理员已经安装好了。若读者机器上没有gcc-9.2，可以找管理员帮忙安装，也可以[自行安装](https://gitlab.com/jxzou/qcinstall/-/blob/main/Linux%E4%B8%8B%E5%AE%89%E8%A3%85%E9%AB%98%E7%89%88%E6%9C%ACGCC.md)。

# 0. 安装openmpi
在gcc-4.8.5生效的环境下使用Intel编译器编译的openmpi产生的mpic++不支持c++17特性，因此这里我们在gcc-9.2生效的环境下使用Intel编译openmpi。到[官网](https://www.open-mpi.org/software/ompi/v4.1)下载压缩包，笔者下载的压缩包为openmpi-4.1.1.tar.bz2。解压，编译

```
module load gcc/9.2

tar -jxf openmpi-4.1.1.tar.bz2
cd openmpi-4.1.1
./configure --prefix=$HOME/software/openmpi-4.1.1 FC=ifort CC=icc CXX=icpc
make -j64
make install
```

此处使用64核并行编译。完成后在`~/.bashrc`中写上环境变量
```
export PATH=$HOME/software/openmpi-4.1.1/bin:$PATH
export LD_LIBRARY_PATH=$HOME/software/openmpi-4.1.1/lib:$LD_LIBRARY_PATH
export CPATH=$HOME/software/openmpi-4.1.1/include:$CPATH
```
退出重新登录，使环境变量生效。此版openmpi亦可用于量化软件ORCA的并行计算。

# 1. 下载
到CP2K依赖库[汇总网站](https://www.cp2k.org/static/downloads)下载压缩包，笔者下载的压缩包名称分别为
```
elpa-2023.05.001.tar.gz
fftw-3.3.10.tar.gz
libgrpp-main-20231215.zip
libxsmm-1.17.tar.gz
OpenBLAS-0.3.25.tar.gz
spglib-1.16.2.tar.gz
```

到GitHub下载CP2K
```
https://github.com/cp2k/cp2k/releases
```
压缩包名称为`cp2k-2024.1.tar.bz2`。当然也可以到这些库的官网去下载相同名称的压缩包。注意CP2K-2024.1内置了这些库的版本号，如果读者下载其他版本压缩包，则编译CP2K时无法识别压缩包名称，编译工具链脚本会试图联网下载符合要求的库，那就达不到本文离线安装的目的了。原则上这些库可以逐一手动编译，这里我们就不费这功夫了，把大部分库留到最后让CP2K的工具链自行编译。若不需要plumed库（本文暂未测试plumed），则无需下载plumed和gsl库。在编译CP2K过程中，LibXC和Libint库是经常碰到报错的环节，因此我们单独编译这两个库，以便遇到报错时方便解决。

# 2. 编译LibXC
依次运行
```
tar -zxf libxc-6.2.2.tar.gz
cd libxc-6.2.2
mkdir build && cd build

CC=icc FC=ifort cmake -DCMAKE_INSTALL_PREFIX=$HOME/software/libxc_6_2_2 \
 -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=NO \
 -DCMAKE_INSTALL_LIBDIR=lib -DENABLE_FORTRAN=ON -DENABLE_PYTHON=OFF \
 -DDISABLE_KXC=OFF -DDISABLE_LXC=ON -DBUILD_TESTING=NO ..

make -j24
make install
```

成功后，在`~/.bashrc`文件里写上环境变量
```
# LibXC
export LD_LIBRARY_PATH=$HOME/software/libxc_6_2_2/lib:$LD_LIBRARY_PATH
export CPATH=$HOME/software/libxc_6_2_2/include:$CPATH
```
退出重登，以使环境变量生效。压缩包和文件夹libxc-6.2.2无用，可以删掉。

# 3. 编译Libint
下载[CP2K特供版Libint](https://github.com/cp2k/libint-cp2k/releases)，笔者下载的是libint-v2.7.0-beta.5-cp2k-lmax-5.tgz，对于绝大多数第一性原理计算足够用了。最高角动量数字4/5/6越大，编译时间越长。解压，进入目录
```
tar -zxf libint-v2.7.0-beta.5-cp2k-lmax-5.tgz
cd libint-v2.7.0-beta.5-cp2k-lmax-5/
```

开始编译，依次运行
```
CC=icc CXX=icpc FC=ifort cmake . -DCMAKE_INSTALL_PREFIX="$HOME/software/libint_2_7_0" \
 -DLIBINT2_INSTALL_LIBDIR="$HOME/software/libint_2_7_0/lib" \
 -DENABLE_FORTRAN=ON -DREQUIRE_CXX_API=OFF

cmake --build . -j 64
cmake --build . --target install
```

此处笔者使用64核并行编译，读者请根据自己机子实际情况修改。完成后在`~/.bashrc`文件里写上
```
# LIBINT
export CPATH=$HOME/software/libint_2_7_0/include:$CPATH
export LD_LIBRARY_PATH=$HOME/software/libint_2_7_0/lib:$LD_LIBRARY_PATH
```
退出重登，以使环境变量生效。同理可删除libint压缩包和解压文件夹，只需保留目录libint_2_7_0。

# 4. 编译CP2K
到toolchain目录下
```
cd $HOME/software/cp2k-2024.1/tools/toolchain
mkdir build && cd build
```

然后将第1步中下载好的6个压缩包挪到`build/`目录下。注意CP2K需要利用OpenBLAS中的一个工具识别机器架构，所以此处6个压缩包里有一个是OpenBLAS。而后我们可以选择Intel MKL作为CP2K的线性代数库，并不需要真的编译OpenBLAS。依次运行
```
cd ..
module load gcc/9.2

./install_cp2k_toolchain.sh --with-gcc=no --with-intel=system --with-cmake=system \
 --with-openmpi=system --with-openblas=no --math-mode=mkl --with-mkl=system \
 --with-scalapack=system --with-libint=system --with-fftw=install --with-libxc=system \
 --with-sirius=no --with-cosma=no --with-libvori=no -j 64
```
如果有报错，应当先处理报错；若一切正常没报错，接着运行
```
cp install/arch/* ../../arch/
source install/setup
cd ../..
make -j64 ARCH=local VERSION="ssmp psmp" >make.log 2>&1
```

此处亦使用64核并行编译。写`=system`的表示使用系统中已编译好的库。注意仔细观察屏幕上各个库的识别情况。此处我们将编译CP2K本体的过程输出到make.log文件中，万一编译报错，可查看该文件中的报错信息。成功后可在`cp2k-2024.1/exe/local/`目录下找到一堆可执行文件，例如`cp2k.ssmp`，`cp2k.psmp`等等。在`~/.bashrc`文件中写入环境变量

```
# CP2K
source $HOME/software/cp2k-2024.1/tools/toolchain/install/setup
export PATH=$HOME/software/cp2k-2024.1/exe/local:$PATH
```
退出重登，以使环境变量生效。

# 5. 测试
我们到晶体开放数据库(COD)下载一个实际的金刚石单胞测试一下
```
http://crystallography.net/cod
```
点击左边Search，在text里输入diamond，在chemical formula输入元素C，点击Send，例如下载COD ID为9011575的cif文件。启动Multiwfn，载入9011575.cif文件，输入cp2k 4个字母进入CP2K输入文件产生功能，指定文件名为diamond.inp，按-1再按4把任务类型修改为变胞优化，最后按数字0即可产生CP2K输入文件。原则上应测试各项参数的收敛性，不过我们此处是程序正常性测试，就不讲究了。可打开inp文件检查一下结构合理性，然后提交CP2K任务
```
mpirun -np 24 cp2k.popt diamond.inp >diamond.out 2>&1 &
```
即使用24个MPI进程并行计算。体系简单，优化四、五步就正常结束了。为使用方便，可在`~/.bashrc`中定义别名
```
alias cp2k='mpirun -np 24 cp2k.popt'
```
以后简单运行
```
cp2k diamond.inp >diamond.out 2>&1 &
```
即可。但要注意alias别名无法在Shell脚本里被识别，若使用脚本提交CP2K计算任务（尤其是在集群上），记得同时在脚本里也写上该别名的定义。

# 6. 可能遇到的问题
在第4小节中执行make后可能会出现如下报错
```
/public/home/jxzou/software/cp2k-2024.1/src/pw/pw_methods.F(1738): error #6752: Since the OpenMP* DEFAULT(NONE) clause applies, the PRIVATE, SHARED, REDUCTION, FIRSTPRIVATE, or LASTPRIVATE attribute must be explicitly specified for every variable.   [NGPTS]
            DO gpt = 1, ngpts
------------------------^
/public/home/jxzou/software/cp2k-2024.1/src/pw/pw_methods.F(1750): error #6752: Since the OpenMP* DEFAULT(NONE) clause applies, the PRIVATE, SHARED, REDUCTION, FIRSTPRIVATE, or LASTPRIVATE attribute must be explicitly specified for every variable.   [NGPTS]
            DO gpt = 1, ngpts
------------------------^
```

解决办法：到如下目录
```
cd cp2k-2024.1/src/pw/
```
打开`pw_methods.F`文件，在OpenMP并行参数` SHARED(c, pw`括号中末尾添加`ngpts`（共有12处这种修改），例如
```
!$OMP PARALLEL DO PRIVATE(gpt, l, m, n) DEFAULT(NONE) SHARED(c, pw, scale)
```
修改为
```
!$OMP PARALLEL DO PRIVATE(gpt, l, m, n) DEFAULT(NONE) SHARED(c, pw, scale, ngpts)
```

然后回到对应目录，重新执行make
```
cd $HOME/software/cp2k-2024.1/
make -j64 ARCH=local VERSION="ssmp psmp" >make.log 2>&1
```


## 相关阅读
[Linux下安装高版本GCC](https://gitlab.com/jxzou/qcinstall/-/blob/main/Linux%E4%B8%8B%E5%AE%89%E8%A3%85%E9%AB%98%E7%89%88%E6%9C%ACGCC.md)  
[Linux下安装Intel oneAPI](https://mp.weixin.qq.com/s/7pQETkrDO1C83vQjKQqI4w)  
[安装Open-MPI](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E5%AE%89%E8%A3%85Open-MPI.md)  
[离线安装CP2K-2022.2_Intel编译器版](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E7%A6%BB%E7%BA%BF%E5%AE%89%E8%A3%85CP2K-2022.2_Intel%E7%BC%96%E8%AF%91%E5%99%A8%E7%89%88.md)

