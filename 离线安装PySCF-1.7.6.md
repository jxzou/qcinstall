由于PySCF程序更新较为频繁，不少小伙伴反映按照我之前在量子化学微信公众号上发的
[离线安装PySCF程序（1.5及更高版本）](https://mp.weixin.qq.com/s/QZPG5jRFZ5s1it2OaBGJqw)
经常会出现编译无法通过等问题，因此再次更新一下安装教程。作为一款量子化学软件，PySCF现在十分强大，支持的功能非常多，此处随意列举几个：

RI加速的HF和CASSCF功能  
二分量、四分量相对论Hartree-Fock  
与其他程序有方便的接口进行DMRG, FCIQMC, SHCI等计算  
多参考微扰方法SC-NEVPT2  
各类EOM方法IP/EA/EE-EOM-CCSD  
CCSD(T)解析梯度  
DFT二阶解析导数、TDDFT解析梯度  
ADC激发态方法  
G0W0近似方法  
支持分子和周期性体系的NAO, IAO, IBO轨道及PM局域化等方法  

在可预见的将来很可能会成为使用人数仅次于Gaussian和ORCA的量子化学软件。当然，PySCF还是免费开源的，有不少开发者基于它开发程序或插件。关于PySCF更多特点介绍请见
```
https://github.com/pyscf/pyscf/blob/master/FEATURES
```

注意PySCF联网在线安装只需`pip install pyscf`一行命令即可。本文介绍的是离线安装步骤，适合不允许联网或很难联网的内部节点。读者在开始编译前需确认自己机子上有gcc和g++编译器，有MKL数学库，以及cmake软件。运行如下命令可查看自己机子上是否存在
```
which gcc
which g++
which cmake
cmake --version (查看版本号)
echo $MKLROOT
```
笔者撰文时用的gcc版本为4.8.5（更高版本当然也可以），cmake版本为3.19（不能低于3.5），MKL数学库用的是Intel Parallel Studio XE 2019 update 5里的。读者机子上若缺少任一前提条件请自行安装。

# 1.下载  
先到GitHub网站下载PySCF压缩包`https://github.com/pyscf/pyscf/releases`，此处我们以1.7.6版本为例，下载压缩包pyscf-1.7.6.tar.gz。解压，进入lib目录
```
tar -zxf pyscf-1.7.6.tar.gz
cd pyscf-1.7.6/pyscf/lib
```
打开此目录下的CMakeLists.txt文件，搜索URL或GIT_REPOSITORY可以看到所需三个库的网址和版本号。笔者将其摘选出，展示如下
```
GIT_REPOSITORY https://github.com/sunqm/libcint.git
GIT_TAG v4.0.7

URL https://gitlab.com/libxc/libxc/-/archive/4.3.4/libxc-4.3.4.tar.gz

GIT_REPOSITORY https://github.com/fishjojo/xcfun.git
GIT_TAG cmake-3.5
```
显然我们到对应网址去下载对应版本的压缩包即可。有GIT_TAG标签的表示需要到Tag界面点击下载对应的版本。具体做法是先点击Releases，然后点击Tags  
![release screenshot](doc/pic1-pyscf-1.7.6.png)  
![tag screenshot](doc/pic2-pyscf-1.7.6.png)  
就能找到相应的版本了。以后面对更新的PySCF版本，只要按照这种方式寻找所需库，就不会在安装过程中碰到缺库或无法兼容的报错。例如，笔者按照这些网址下载下来的压缩包分别是
```
libcint-4.0.7.tar.gz
libxc-4.3.4.tar.gz
xcfun-cmake-3.5.tar.gz
```
其实这三个库的安装步骤在CMakeLists.txt文件里也有写。为了更清晰地展示安装过程，以下也逐一贴出.

# 2.编译libcint  
到存放压缩包的目录下，依次执行
```
tar -zxf libcint-4.0.7.tar.gz
cd libcint-4.0.7
mkdir build && cd build

cmake -DWITH_F12=1 -DWITH_RANGE_COULOMB=1 -DWITH_COULOMB_ERF=1 \
-DMIN_EXPCUTOFF=20 -DKEEP_GOING=1 \
-DCMAKE_INSTALL_PREFIX:PATH=/home/$USER/software/cint_and_xc \
-DCMAKE_INSTALL_LIBDIR:PATH=lib ..

make && make install
```
这里的库存放路径`/home/$USER/software/cint_and_xc`是笔者的个人偏好，读者可以按照自己的喜好或需求更改。

# 3.编译libxc  
到存放压缩包的目录下，依次执行
```
tar -zxf libxc-4.3.4.tar.gz
cd libxc-4.3.4/build

cmake -DCMAKE_BUILD_TYPE=RELEASE -DBUILD_SHARED_LIBS=1 \
-DCMAKE_INSTALL_PREFIX:PATH=/home/$USER/software/cint_and_xc \
-DCMAKE_INSTALL_LIBDIR:PATH=lib ..

make -j8
make install
```
其中-j8表示8核并行编译，若读者机子上没这么多核，请减小核数或直接删去这一参数。

# 4. 编译xcfun  
到存放压缩包的目录下，依次执行
```
tar -zxf xcfun-cmake-3.5.tar.gz
cd xcfun-cmake-3.5
mkdir build && cd build

cmake -DCMAKE_BUILD_TYPE=RELEASE -DBUILD_SHARED_LIBS=1 \
-DXCFUN_MAX_ORDER=3 \
-DCMAKE_INSTALL_PREFIX:PATH=/home/$USER/software/cint_and_xc \
-DCMAKE_INSTALL_LIBDIR:PATH=lib ..

make -j8
make install
```
上述四步完成后，可以在`/home/$USER/software/cint_and_xc`下发现有`bin`, `include`, `share`和`lib`四个文件夹。接着将lib路径添加进环境变量，即打开~/.bashrc文件写入
```
export LD_LIBRARY_PATH=/home/$USER/software/cint_and_xc/lib:$LD_LIBRARY_PATH
```
然后source使之生效。

# 5.安装PySCF  
我们回到本文一开始提到的`pyscf-1.7.6/pyscf/lib`目录，再次打开CMakeLists.txt文件，找到第一个`# set(BLAS_LIBRARIES "-L/`这一行，在此处删除注释符号`# `，更改mkl库路径为当前系统下的mkl路径，例如笔者机子上的是`/opt/intel/mkl/lib/intel64`，注意别把前头的-L删了，后面那些-lmkl_xxx也不要删。下面几行`or`部分不用动。接着执行
```
mkdir build && cd build
cmake -DBUILD_LIBCINT=0 -DBUILD_LIBXC=0 -DBUILD_XCFUN=0 \
-DCMAKE_INSTALL_PREFIX:PATH=/home/$USER/software/cint_and_xc ..

make
```
安装完成后将PySCF的路径添加进~/.bashrc文件：
```
export PYTHONPATH=/home/$USER/software/pyscf-1.7.6:$PYTHONPATH
```
完成安装。安装包和解压出的文件夹都可以删除，只留下`cint_and_xc`和`pyscf-1.7.6`文件夹即可。最后同样要记得执行`source ~/.bashrc`，或者退出重登。

# 6.测试例子  
随便测试个CCSD(T)计算。新建一个文件a.py，写入
```
from pyscf import gto, scf, cc
mol = gto.M(atom='H 0 0 0; F 0 0 1.1',basis='ccpvdz')
mf = scf.RHF(mol)
mf.kernel()

mycc = cc.CCSD(mf)
mycc.frozen = 1
mycc.kernel()

mycc.ccsd_t()
```
保存。运行
```
python a.py
```
输出
```
converged SCF energy = -99.9873974403487
E(CCSD) = -100.2018830963878  E_corr = -0.2144856560390928
CCSD(T) correction = -0.00239622928398965
```
读者可以自行用高斯做个计算对比。注意高斯在电子相关计算中默认冻结芯轨道，而PySCF默认不冻结。对于氟化氢这个例子需要冻结的轨道只有1个，即F原子的1s轨道，因此这里显式地设定了`mycc.frozen = 1`让其与高斯一致。

# 7.可能遇到的问题  
如果运行时报错找不到库mkl_def.so和mkl_avx2.so，可回到上述第5点中再次打开CMakeLists.txt文件，在`-lmkl_avx`后添加`-lmkl_def -lmkl_avx2`，保存。进入build目录，重复一次之前的cmake和make操作。

## 相关阅读
[离线安装PySCF-2.x](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E7%A6%BB%E7%BA%BF%E5%AE%89%E8%A3%85PySCF-2.x.md)
