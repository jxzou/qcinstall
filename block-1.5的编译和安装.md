由于CASCI/CASSCF计算量随活性空间呈指数增长，超过(16,16)的计算在高配机器上几乎不可能。近似求解大活性空间的方法通常有DMRG, selected CI等等。Block-1.5是做DMRG计算的经典程序，由Sandeep Sharma和Garnet Chan开发，虽然早在5年前就不更新了，但其计算速度仍高于很多同类程序。Block-1.5一般结合PySCF使用，可以进行DMRG-CASCI，DMRG-CASSCF和DMRG-SC-NEVPT2等计算。笔者之前在公众号上将该程序的安装拆分为几篇短文

[Boost.MPI的编译](https://mp.weixin.qq.com/s/AMYUTB5pTNLFZ8NEtFIG-Q)  
[安装基于openmpi的mpi4py](https://mp.weixin.qq.com/s/f5bqgJYG5uAK1Zubngg65g)  
[Block-1.5的编译和安装](https://mp.weixin.qq.com/s/EUZKLYSqbuIUL9-zlySfbQ)

不便统筹阅读，加上以前的教程有些细微的格式问题，有必要整理和汇总一下安装步骤。

# 1. 编译boost.MPI
以boost.1.55.0为例，压缩包boost_1_55_0.tar.gz可以在[官网](http://www.boost.org/users/history)下载到。注意block-1.5.3对boost版本较为敏感，笔者不推荐使用1.55.0外的版本。有些读者可能会发现自己机器上系统内置了（部分）boost库，但其一般无法用于编译Block。

为了编译出并行版的Boost库，需要事先安装任一种mpi，本文以openmpi-1.10.3为例（若读者未安装，可阅读[此文](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E5%AE%89%E8%A3%85Open-MPI.md)）。假设压缩包放在`$HOME/software/`目录下，解压，进入目录，运行bootstrap.sh脚本并设置相关参数
```
tar -zxf boost_1_55_0.tar.gz
cd boost_1_55_0
./bootstrap.sh --prefix=`pwd` --with-libraries=all --with-toolset=intel-linux
```

这里通过`--with-toolset`来指定编译器，写`intel-linux`就是intel编译器，写`gcc`就是gcc编译器。该选择**建议与当初编译openmpi的编译器保持一致**。`pwd`指安装在当前文件夹下，也可以更改至其他位置。打开`project-config.jam`文件，在第一行添加mpicxx的绝对路径
```
using mpi : /opt/openmpi-1.10.3/bin/mpicxx ;
```
注意最后有一个分号不能少。这一行内容对空格较敏感，建议读者复制后修改。如果不知道`mpicxx`路径，可通过命令`which mpicxx`查询。然后编译
```
./b2 -j16 --layout=tagged link=static,shared threading=multi install
```

可以通过加上-j16使用16个核并行编译，大概3分钟即可完成。在lib文件夹下可以看到生成libboost_mpi-mt.so等库文件。mt是multi-thread的意思；linux系统自带的boost库里通常没有这些并行库文件。假设boost放在`$HOME/software/`目录下，则需添加环境变量
```
export BOOST_ROOT=$HOME/software/boost_1_55_0
export BOOST_INCLUDE=$BOOST_ROOT/include:$BOOST_INCLUDE
export BOOST_LIB=$BOOST_ROOT/lib:$BOOST_LIB
export LD_LIBRARY_PATH=$BOOST_ROOT/lib:$LD_LIBRARY_PATH
```
第一行的路径视自己的实际情况修改。`source ~/.bashrc`或退出重登以使环境变量生效。

# 2. 编译block-1.5.3
原网址
```
http://www.sunqm.net/pyscf/files/bin/block.spin_adapted-1.5.3.gz
```
在几年前已失效。现可到下列两个链接下载源码
```
https://github.com/pyscf/Block
https://github.com/sanshar/Block/files/10379436/block-1.5.3.tar.gz
```
笔者使用的压缩包为block-1.5.3.tar.gz。

## 2.1 编译并行版
解压，复制一份代码，这是因为在2.2节中可能还需编译串行版，此处复制一份以区分二者
```
tar -zxf block-1.5.3.tar.gz
cp -r block-1.5.3 block-1.5.3_mpi
cd block-1.5.3_mpi
```

打开`Makefile`文件，找到并修改如下几处
```
CXX = icpc
MPICXX = mpicxx
BOOSTINCLUDE = $(BOOST_ROOT)/include/
BOOSTLIB = -L$(BOOST_ROOT)/lib -lboost_system-mt -lboost_filesystem-mt -lboost_serialization-mt -lboost_mpi-mt

USE_MPI = yes
USE_MKL = yes
MKLLIB = $(MKLROOT)/lib/intel64
MKLFLAGS = $(MKLROOT)/include
```

在`Makefile`中找到如下这一行
```
LIBS +=  -L$(NEWMATLIB) -lnewmat $(BOOSTLIB) $(LAPACKBLAS) $(MALLOC)
```
在其末尾添加`-lpthread -lrt`，也即
```
LIBS +=  -L$(NEWMATLIB) -lnewmat $(BOOSTLIB) $(LAPACKBLAS) $(MALLOC) -lpthread -lrt
```

找到这三行内容
```
ifeq (icpc, $(CXX))
   ifeq ($(OPENMP), yes)
      OPENMP_FLAGS= -openmp
```
将其中`-openmp`改成`-qopenmp`，因为近几年Intel编译器不再支持`-openmp`，而是`-qopenmp`。

找到这一行内容
```
     MPI_LIB = -lboost_mpi
```
将其注释，也即
```
#     MPI_LIB = -lboost_mpi
```

鉴于修改处较多，笔者建议读者在其他机器上安装时复制此修改好的`Makefile`，以免出错。终于可以开始编译，笔者使用了4核并行编译
```
make -j4
```

无需make install步骤。完成后可以运行
```
./block.spin_adapted -v
```
显示版本。笔者机子上显示的是
```
Runing with 1 processors
Block 1.5
Copyright (C) 2012  Garnet K.-L. Chan
This program comes with ABSOLUTELY NO WARRANTY; for details see license file.
This is free software, and you are welcome to redistribute it
under certain conditions; see license file for details.
```

## 2.2 编译串行版（可选）
若读者只想要DMRG-CASCI和DMRG-CASSCF计算、而不需要DMRG-NEVPT2计算，则用不到此串行版，可跳过此段落。进入block-1.5.3目录，打开`Makefile`文件，找到并修改如下几处
```
CXX = icpc
BOOSTINCLUDE = $(BOOST_ROOT)/include/
BOOSTLIB = -L$(BOOST_ROOT)/lib -lboost_system-mt -lboost_filesystem-mt -lboost_serialization-mt -lboost_mpi-mt

USE_MKL = yes
MKLLIB = $(MKLROOT)/lib/intel64
MKLFLAGS = $(MKLROOT)/include
```

还有`-lpthread -lrt`和`-qopenmp`两处修改，在2.1中已提到。同样执行`make -j4`编译。

# 3. 编译mpi4py（可选）
若读者只想要DMRG-CASCI和DMRG-CASSCF计算、而不需要DMRG-NEVPT2计算，则用不到mpi4py，可跳过此段落。到[GitHub](https://github.com/mpi4py/mpi4py/releases)下载压缩包，笔者下载的是mpi4py-3.0.0.tar.gz，解压，进入目录
```
tar -zxf mpi4py-3.0.0.tar.gz
cd mpi4py-3.0.0
```

打开配置文件mpi.cfg，修改其中的[openmpi]处信息为
```
[openmpi]
mpi_dir              = /opt/openmpi-1.10.3
mpicc                = %(mpi_dir)s/bin/mpicc
mpicxx               = %(mpi_dir)s/bin/mpicxx
#include_dirs         = %(mpi_dir)s/include
#libraries            = mpi
library_dirs         = /opt/intel/compilers_and_libraries/linux/lib/intel64
runtime_library_dirs = %(library_dirs)s
```

注意mpi4py依赖于openmpi，上文介绍过笔者电脑上装的openmpi位于`/opt/openmpi-1.10.3`，读者请按照自己机器实际情况修改路径。若openmpi是使用Intel OneAPI编译的，则应修改为
```
library_dirs         = /opt/intel/oneapi/compiler/latest/linux/compiler/lib/intel64
```
保存修改，执行
```
python setup.py build --mpi=openmpi
```
完成编译。接着选择安装位置，有3种选择：

（1）安装在自己的目录下
```
python setup.py install --user
```
这样会安装到`~/.local/`文件夹下，由于其位置特殊，不需要在~/.bashrc里增添mpi4py的环境变量，因为`~/.local/`下的文件会自动被识别。

（2）安装至其他指定路径
例如
```
python setup.py install --prefix=$HOME/software/mpi4py-3.0.0
```
此时需要自己在~/.bashrc文件里添加其环境变量
```
export PYTHONPATH=$HOME/software/mpi4py-3.0.0/lib/python3.7/site-packages:$PYTHONPATH
```

（3）安装在系统的anaconda文件夹里（如/opt/anaconda3，需要root权限）
```
python setup.py install --prefix=/opt/anaconda3
```
它其实是自动安装进`/opt/anaconda3/lib/python3.7/site-packages/`里去，与numpy和scipy同级的目录下。mpi4py在其目录下可以被自动识别，不需要再添加环境变量。测试是否安装成功，启动python
```python
from mpi4py import MPI
```
若无报错则安装成功。压缩包mpi4py-3.0.0.tar.gz和解压得到的文件夹mpi4py-3.0.0无用，均可删除。

# 4. 在PySCF中设定Block-1.5路径
若读者尚未安装PySCF，请见文末相关链接，自行安装。若读者安装的是PySCF-1.7.6，那就进入`pyscf/pyscf/dmrgscf/`目录；若安装的是PySCF-2.x和dmrgscf插件，那就进入`dmrgscf/pyscf/dmrgscf/`目录。注意目标目录可能随下载包来源的不同而不同，例如也有可能叫`dmrgscf-master/pyscf/dmrgscf/`。到目录中找到settings.py.example文件，复制一份
```
cp settings.py.example settings.py
```
打开`settings.py`文件，修改如下几处
```
BLOCKEXE = '/opt/block-1.5.3_mpi/block.spin_adapted'
BLOCKEXE_COMPRESS_NEVPT = '/opt/block-1.5.3/block.spin_adapted'
BLOCKSCRATCHDIR = os.path.join('/scratch/jxzou/pyscf', str(os.getpid()))
```
第一行需填写MPI版Block路径，第二行需填写串行版Block路径，第三行是临时文件路径，这些路径请读者根据本地情况修改。前已提过，若无需DMRG-NEVPT2计算，则无需编译串行版Block，自然也不用管`BLOCKEXE_COMPRESS_NEVPT`。为使程序读写临时文件更快，应写为大容量分区的目录或固态硬盘。

# 5. 测试
PySCF与Block-1.5联用的输入文件示例见`dmrgscf/examples/`目录。此处笔者展示一个MOKIT调用PySCF和Block-1.5自动做多参考态计算的例子，分子为

<img src="doc/cyclobuta_b_anthracene.png" width="60%" height="60%" />

几何结构先用CAM-B3LYP/6-31G(d,p)级别优化一下。对于这种共轭分子，多组态/多参考态计算最常用的活性空间大小为全部pi轨道，此处即(16,16)，包含8个C-C pi成键轨道和8个C-C pi*反键轨道。使用MOKIT自动做计算的话，我们就不用自己去挑选轨道了，输入文件如下
```
%mem=160GB
%nprocshared=24
#p NEVPT2/cc-pVDZ

mokit{npair=8}

0 1
C        -4.24566100     0.70775600     0.00000200
C        -4.24566100    -0.70775600     0.00000100
C        -3.06469600    -1.39740500     0.00000000
C        -1.82221000    -0.71140300     0.00000000
C        -1.82221000     0.71140300     0.00000000
C        -3.06469600     1.39740500     0.00000100
C        -0.58525800    -1.39038300    -0.00000100
C         0.61987500    -0.72352400    -0.00000100
C         0.61987500     0.72352400    -0.00000100
C        -0.58525800     1.39038300    -0.00000100
C         1.88409800    -1.45948300     0.00000200
C         3.00192300    -0.72943000    -0.00000600
C         3.00192300     0.72943000    -0.00000900
C         1.88409800     1.45948300    -0.00000100
C         4.49498500    -0.67672300     0.00000200
C         4.49498500     0.67672300     0.00000700
H        -5.18939000     1.24342300     0.00000300
H        -5.18939000    -1.24342300     0.00000200
H        -3.06023600    -2.48359300     0.00000000
H        -3.06023600     2.48359300     0.00000000
H        -0.58866800    -2.47748700    -0.00000100
H        -0.58866800     2.47748700     0.00000000
H         1.86281500    -2.54477900     0.00000800
H         1.86281500     2.54477900     0.00000400
H         5.27714000    -1.42481000     0.00000600
H         5.27713900     1.42481000     0.00001600

```

这个任务在高配置节点上强行使用CASSCF(16,16)硬算亦可，但此处我们希望算NEVPT2，基于传统CASSCF无望，因此先将参考态改用DMRG-CASSCF(16,16)，并基于此参考态进行DMRG-SC-NEVPT2计算，最大保留态maxM=1000（这些MOKIT都会自动处理）。提交任务
```
automr cyclobuta_b_anthracene.gjf >cyclobuta_b_anthracene.out 2>&1 &
```
使用24核计算耗时约3.5 h。可分别打开`*_s.fch`或`*_CASSCF_NO.fch`文件检查GVB自然轨道或CASSCF自然轨道，均为pi轨道（节约篇幅，此处未展示）。打开输出文件看能量结果为
```
E(CASSCF) =      -611.89824962 a.u.
Nevpt2 Energy = -1.891190771954538 a.u.
```

另一方面，我们可以使用Block2程序进行同样的计算，对比结果
```
E(CASSCF) =      -611.89824962 a.u.
Nevpt2 Energy = -1.891189523923513
```

DMRG-CASSCF能量高度一致，NEVPT2相关能相差1e-6 a.u.级别，可以接受，说明程序安装没问题。

# 6. 可能遇到的报错或警告
见[此文](https://gitlab.com/jxzou/qcinstall/-/blob/main/block2%E7%9A%84%E7%BC%96%E8%AF%91%E5%92%8C%E5%AE%89%E8%A3%85.md)2.5节。

# 相关阅读
[离线安装PySCF-1.7.6](https://mp.weixin.qq.com/s/xtlOZ8XvEaL4nAiq7W7LeQ)  
[离线安装PySCF-2.x](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E7%A6%BB%E7%BA%BF%E5%AE%89%E8%A3%85PySCF-2.x.md)  
[离线安装PySCF-2.x-extensions](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E7%A6%BB%E7%BA%BF%E5%AE%89%E8%A3%85PySCF-2.x-extensions.md)  
[block2的编译和安装](https://gitlab.com/jxzou/qcinstall/-/blob/main/block2%E7%9A%84%E7%BC%96%E8%AF%91%E5%92%8C%E5%AE%89%E8%A3%85.md)

