# English
This repository contains many installation tutorials of quantum chemistry software packages, which are mainly written in Chinese by me. Some of tutorials have been shown/published in WeChat Official Account "Quantum Chemistry", but here are the newst version since minor modifications may have been made.

# Chinese
该仓库用于存放本人所写的量子化学程序安装教程，以中文教程为主。部分安装教程曾在微信公众号“量子化学”上展示或发表过，但此处可能经过微小的修改和更新，该仓库中教程为最新版本。

# Questions or Errors
If you have any question about any tutorial, or you find any errors/typos, you can open an issue on the [Issues](https://gitlab.com/jxzou/qcinstall/-/issues) page.

