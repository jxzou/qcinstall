# 1. 引言
MOKIT从v1.2.6rc23版本起已支持量子化学软件Turbomole产生的分子轨道文件的传入和传出，涉及的主要小程序名称为`fch2tm`和`molden2fch`。`fch2tm`有以下功能：

（1）从fch文件直接生成Turbomole输入文件control和轨道文件mos，前者含坐标、基组、赝势（如果用了赝势）和部分简单关键词，后者含轨道数据，无需通过Turbomole自带的`define`模块交互操作，节约时间，适合批处理。用户只需添加一点自己想要的关键词即可开展目标计算。若不作任何修改，默认关键词为相应的RHF/ROHF/UHF任务。

（2）万一在HF/DFT、RI-CC2、TDDFT等计算中发生SCF不收敛、参考态波函数不稳定、SCF收敛到其他解等问题，可先用Gaussian/ORCA等常见量化程序计算、检验好波函数稳定性，然后用`fch2tm`从fch文件传轨道至Turbomole，使其立即收敛，不必在Turbomole中反复尝试，节约人力物力。

`molden2fch`顾名思义，即将Turbomole产生的.molden文件转化为fch文件，用途有：

（1）获得的fch文件可用GaussView/Multiwfn等常用软件进行可视化，方便，还可利用Multiwfn大量的波函数分析功能。

（2）获得的fch文件可进一步传给其他量子化学程序，例如使用`fch2mkl`传给ORCA，`fch2qchem`传给Q-Chem，`fch2com`传给Molpro，`fch2inporb`传给(Open)Molcas等等，尽量避免在其他程序中进行重复计算，甚至可联用多个程序、经过修改开发新的理论方法。


# 2. fch2tm使用示例
举一个UHF的例子，gjf文件内容如下
```
%chk=hocl.chk
%mem=128GB
%nprocshared=64
#p UHF/aug-cc-pVTZ nosymm int=nobasistransform scf(maxcycle=128,xqc) stable=opt units=au

title

-1 2
O   -2.08303774     0.11989815     0.0
H   -2.75920696    -1.56351799     0.0
Cl   1.03231321    -0.00978036     0.0

```

我们在Gaussian中做好UHF计算，检验好波函数稳定性，获得chk/fch文件，然后传轨道给Turbomole，即运行
```
formchk hocl.chk hocl.fch
fch2tm hocl.fch
```
随即产生`control`和`mos`文件，啥也不用改，提交给Turbomole，即运行
```
dscf >OUTPUT 2>&1
```

可发现Turbomole立即收敛
```
 ITERATION  ENERGY          1e-ENERGY        2e-ENERGY     NORM[dD(SAO)]  TOL
   1  -534.89646998125    -846.68677835     259.61463347    0.000D+00 0.240D-11
          max. resid. norm for Fia-block=  7.830D-09 for orbital      1a    beta
          max. resid. fock norm         =  1.463D-08 for orbital      1a    alpha
          Delta Eig. =     0.0000021950 eV

                                              current damping :  0.000
 ITERATION  ENERGY          1e-ENERGY        2e-ENERGY     NORM[dD(SAO)]  TOL
   2  -534.89646998121    -846.68677835     259.61463347    0.193D-06 0.152D-11
          Norm of current diis error: 0.59401E-08
          max. resid. norm for Fia-block=  8.292D-10 for orbital      9a    alpha
          max. resid. fock norm         =  4.719D-09 for orbital    119a    alpha
          Delta Eig. =     0.0000000980 eV

 ENERGY CONVERGED !


                                              current damping :  0.000
 ITERATION  ENERGY          1e-ENERGY        2e-ENERGY     NORM[dD(SAO)]  TOL
   3  -534.89646998116    -846.68677835     259.61463347    0.152D-07 0.115D-11
          max. resid. norm for Fia-block=  1.801D-09 for orbital     14a    alpha
          max. resid. fock norm         =  1.422D-08 for orbital     93a    alpha

 convergence criteria satisfied after  3 iterations
```
Gaussian的UHF能量是-534.896469981 a.u. 而Turbomole的SCF第1圈使用读入的轨道计算电子能量，可以看到一开始的能量就是收敛值；但其需要计算第2圈以判断两圈之差小于收敛阈值。收敛后还会额外进行一圈计算，因此虽然传入的轨道是收敛的，仍然需要进行3圈SCF计算。有的量化程序把这里的第1圈称作第0圈，也有的程序在收敛后不进行额外1圈迭代，看上去就可能是1圈收敛，本质上没太大不同。`fch2tm`小程序传轨道时最高支持到h角动量（相当于C原子的cc-pV5Z基组），正确考虑了基函数的顺序问题，支持不同元素使用混合基组、赝势等，目前仅支持球谐型基函数，因此请不要在gjf文件中写`6D 10F`（即勿使用Cartesian型基函数）。


# 3. molden2fch使用示例
Turbomole在正常计算完成后会更新`control`和`mos`文件，此时我们可以执行
```
tm2molden
test.molden
[Enter]
[Enter]
```
便可产生`test.molden`文件，其中`tm2molden`是Turbomole自带的小程序，`[Enter]`表示键盘按回车键。然后运行
```
molden2fch test.molden -tm
```
可获得test.fch文件。细心的读者可能会注意到屏幕有提示信息
```
Warning from subroutine molden2fch: molden file does not include spin multiplicity.
It will be guessed according to the occupation numbers. If the guessed spin is wrong,
you need to modify it in file test.fch
```
这是因为molden文件不含体系的电荷和自旋多重度信息（而Gaussian的fch文件含有电荷和自旋多重度信息），`molden2fch`只能根据体系轨道占据数和电子数进行猜测，对于HF/DFT任务往往不会猜错。若读者进行复杂任务，或者传的是自然轨道，需注意fch文件中的电荷和自旋多重度是否正确。这两项信息正确与否对观看轨道形状、分析轨道成份均无影响，但若用此fch文件进行后续计算或传轨道给其他程序，可能造成信息错误。值得注意的是，目前诸多量子化学程序产生的molden文件，其格式和基函数顺序并不严格遵循Molden文件格式标准，而`molden2fch`根据每个量化程序正确地考虑了基函数的顺序，需要用户指定参数`-tm`告诉`molden2fch`这是Turbomole导出的molden文件。类似地，如下命令
```
orca_2mkl h2o -molden
molden2fch h2o.molden -orca
```
可将ORCA的molden文件正确转化为fch文件。注意基函数角动量超过g时，Turbomole导出molden文件时会报相应的错误信息，此时`molden2fch`无能为力。由于molden文件不含赝势数据，因此产生的fch文件也不含赝势，读者使用赝势基组时需要注意这点，若使用全电子基组则无此问题。以后`molden2fch`还会支持更多量化程序的molden文件转化，欢迎广大读者使用、提建议和需求。


# 4. 如何安装使用
MOKIT程序开源、免费，其主页见
```
https://gitlab.com/jxzou/mokit
```
支持conda命令在线安装、预编译版的下载、离线安装等多种手段。MOKIT在线手册见
```
https://jeanwsr.gitlab.io/mokit-doc-mdbook/
```
若有读者在发表的研究中使用了MOKIT的功能，请恰当引用之。若使用中有任何问题，欢迎到GitLab issue区提问
```
https://gitlab.com/jxzou/mokit/-/issues
```
引用MOKIT的已发表文章一览
```
https://jeanwsr.gitlab.io/mokit-doc-mdbook/citing.html
```
