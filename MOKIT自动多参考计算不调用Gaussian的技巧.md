随着[MOKIT](https://gitlab.com/jxzou/mokit)用户的增加，有些小伙伴在使用中有如下问题：

（1）我的电脑/计算节点上没安装Gaussian软件，可否让MOKIT不自动调用Gaussian？

（2）我所在单位/课题组被Gaussian公司给ban了，或者我们单位要求禁止使用Gaussian，有何替代方案？

（3）我想少安装/少使用一个量化软件，有什么办法？

（4）我想在CASSCF或NEVPT2计算中使用标量相对论哈密顿sfX2C，Gaussian不支持该哈密顿，也没有内置适配的基组例如x2c-SVPall，如何解决？

我们先解释一下这些问题的来源，以两根O-H键被拉伸的水分子CASSCF计算文件为例

```
%mem=4GB
%nprocshared=2
#p CASSCF/cc-pVDZ

mokit{}

0 1
O      -0.23497692    0.90193619   -0.068688
H       1.26502308    0.90193619   -0.068688
H      -0.73568721    2.31589843   -0.068688

```

MOKIT程序中负责自动多组态/多参考计算的可执行文件是[automr](https://jeanwsr.gitlab.io/mokit-doc-mdbook/chap5-1.html)，提交任务的一般方式是
```
automr h2o.gjf >h2o.out 2>&1
```

对于单重态体系，`automr`第一步会默认调用**Gaussian**软件进行RHF和UHF计算，检验UHF波函数稳定性和确保UHF波函数稳定，检查RHF/UHF能量高低，选择能量较低的波函数进行后续处理。想进行多组态/多参考计算的用户提供的体系一般都有较强或很强的多参考特征，所以基本都是UHF能量显著低于RHF。

接着`automr`会将两列UHF轨道变换为1列UNO轨道（即UHF自然轨道），然后对UNO中的活性占据UNO进行轨道局域化，将局域化获得的酉矩阵复制给非活性占据UNO，获得局域配对UNO轨道，这便是GVB初始轨道。

第二步，`automr`会调用**GAMESS**软件进行GVB计算。GVB计算也是一种SCF过程，上述轨道会被优化。待成功收敛后，自然轨道占据数在[0.02,1.98]之间的GVB轨道会被当做CASSCF活性空间初始轨道，由于每个GVB pair包含2个活性电子，体系的活性电子数也在此时确定；其余轨道分别做为双占据轨道和虚轨道。

第三步，`automr`会调用**PySCF**软件进行CASSCF计算。计算成功后将CASSCF自然轨道和轨道占据数保存进一个xxx_CASSCF_NO.fch文件。对于上述水分子的例子，最终会自动计算出CASSCF(4,4)的结果，活性空间包含2根O-H键的成键轨道和反键轨道。用户打开结果文件查看即可，中间并不需要肉眼看轨道、调换轨道、小基组->大基组投影、解决CASSCF不收敛等繁琐步骤。

**总结起来**，gjf文件中的`mokit{}`实际上隐含了三个默认调用程序
```
mokit{HF_prog=Gaussian,GVB_prog=GAMESS,CASSCF_prog=PySCF}
```

因此很自然地，如果读者不想让`automr`调用Gaussian，可在mokit{}中使用关键词`HF_prog`指定相应的HF计算程序，例如调用PySCF进行HF计算
```
%mem=4GB
%nprocshared=2
#p CASSCF/cc-pVDZ

mokit{HF_prog=PySCF}

0 1
O      -0.23497692    0.90193619   -0.068688
H       1.26502308    0.90193619   -0.068688
H      -0.73568721    2.31589843   -0.068688

```
关键词和程序名称不区分大小写。使用该功能请安装MOKIT-v1.2.6rc26或更高版本。MOKIT具有通用的传轨道功能，即使更换一个（或多个）调用的量化程序，各步计算获得的轨道仍然保存在相应的.fch文件中，不影响后续可视化和分析。传统单个量化程序的计算经常需要捆绑一个相应的可视化程序，一般只有官方编写或推荐的可视化程序才具有相对完备和正确的可视化支持。

也有用户想调用ORCA做HF计算，那可以这么写
```
%mem=4GB
%nprocshared=2
#p CASSCF/cc-pVDZ

mokit{HF_prog=ORCA}

0 1
O      -0.23497692    0.90193619   -0.068688
H       1.26502308    0.90193619   -0.068688
H      -0.73568721    2.31589843   -0.068688

```
使用该功能请安装MOKIT-v1.2.6rc38或更高版本。

上面的例子都是非相对论计算。若想使用sfX2C哈密顿并搭配x2c-SVPall或x2c-TZVPall等相对论全电子基组，可以这么写
```
%mem=4GB
%nprocshared=2
#p CASSCF/x2c-SVPall

mokit{HF_prog=PySCF,X2C}

0 1
O      -0.23497692    0.90193619   -0.068688
H       1.26502308    0.90193619   -0.068688
H      -0.73568721    2.31589843   -0.068688

```
此时偷懒不写`HF_prog=PySCF`也可以。`automr`检测到X2C关键词，知道Gaussian目前不支持，会自动切换为调用PySCF做HF计算。若读者想尝试ORCA 6新支持的sfX2C功能，也可以写`HF_prog=ORCA`，此功能从MOKIT-v1.2.6rc39开始支持。

类似地，`GVB_prog`也可以更换为Gaussian或QChem。`CASSCF_prog`的选择余地更多，支持OpenMolcas, ORCA, Molpro, GAMESS, Gaussian, BDF, PSI4, Dalton等一大堆量化程序，可以满足各种用户的偏好。

不过`automr`各个步骤默认调用的量化程序是世界上对应功能几乎最稳健、最高效的程序，一般不建议读者更换（除非由于个别程序不支持sfX2C哈密顿等原因）。MOKIT默认调用哪个量化程序的原则就是哪个软件的某功能最快最稳健，或者独一无二，就用哪个。随着时代的发展，每隔三、五年可能就有新的强势方法/算法出现，MOKIT默认参数也会做出相应改变。

这里的x2c-SVPall基组在Gaussian和PySCF中没有内置，但MOKIT内置了此基组，可以让用户通过gjf文件语法方便地使用Gaussian没有的功能，节约了用户学习和适应多个量化软件的时间。

如果想使用DKH2标量相对论哈密顿，并搭配合适基组如DKH-def2-SVP，可以这么写
```
%mem=4GB
%nprocshared=2
#p CASSCF/DKH-def2-SVP

mokit{DKH2,CASSCF_prog=ORCA}

0 1
O      -0.23497692    0.90193619   -0.068688
H       1.26502308    0.90193619   -0.068688
H      -0.73568721    2.31589843   -0.068688

```
PySCF目前不支持DKH2，所以这里我们改成ORCA等支持的程序做CASSCF计算。Gaussian同样没有内置DKH-def2-SVP基组，但MOKIT内置了这个基组，因此可顺利调用Gaussian进行HF计算、调用GAMESS进行GVB计算，并开启相应的DKH2哈密顿。

**相关链接**

MOKIT的GitLab仓库
```
https://gitlab.com/jxzou/mokit
```

MOKIT在线手册
```
https://jeanwsr.gitlab.io/mokit-doc-mdbook
```

引用MOKIT程序的已发表文章一览
```
https://jeanwsr.gitlab.io/mokit-doc-mdbook/citing.html
```